import 'package:flutter/material.dart';
import 'package:hive_flutter/hive_flutter.dart';
import 'package:rompos_ticket_registrar/models/user_settings.dart';
import 'package:rompos_ticket_registrar/screens/auth_screen.dart';
import 'package:rompos_ticket_registrar/screens/events_screen.dart';
import 'package:rompos_ticket_registrar/screens/ticket_scanner_screen.dart';
import 'package:rompos_ticket_registrar/screens/verify_tickets_screen.dart';
import 'package:rompos_ticket_registrar/screens/schedules_screen.dart';
import 'package:rompos_ticket_registrar/screens/settings_screen.dart';
import 'package:rompos_ticket_registrar/services/hive_boxes.dart';
import 'package:rompos_ticket_registrar/services/login_auth_service.dart';
import 'package:rompos_ticket_registrar/services/package_info_service.dart';
import 'package:rompos_ticket_registrar/services/profile_service.dart';
import 'package:rompos_ticket_registrar/services/services_locator.dart';
import 'package:provider/provider.dart';

UserSettings? settings;

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Hive.initFlutter();
  Hive.registerAdapter<UserSettings>(UserSettingsAdapter());
  await Hive.openBox<UserSettings>(HiveBoxes.settings);
  runApp(MyApp());
}

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  @override
  void initState() {
    super.initState();
    setupLocator();
    settings = getIt<ProfileService>().getSettings();
  }

  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider<LoginAuthService>(
            create: (_) => getIt<LoginAuthService>()),
        ChangeNotifierProvider<PackageInfoService>(
            create: (_) => getIt<PackageInfoService>()),
      ],
      child: MaterialApp(
        navigatorKey: GlobalKey<NavigatorState>(),
        theme: ThemeData(
          primarySwatch: Colors.blueGrey,
          visualDensity: VisualDensity.adaptivePlatformDensity,
        ),
        debugShowCheckedModeBanner: false,
        home: Consumer<LoginAuthService>(
          builder: (BuildContext context, service, _) {
            return service.token.isNotEmpty ? EventsScreen() : AuthScreen();
          },
        ),
        routes: {
          EventsScreen.routeName: (ctx) => EventsScreen(),
          SchedulesScreen.routeName: (ctx) => SchedulesScreen(),
          TicketScannerScreen.routeName: (ctx) => TicketScannerScreen(),
          VerifyTicketsScreen.routeName: (ctx) => VerifyTicketsScreen(),
          SettingsScreen.routeName: (ctx) => SettingsScreen(),
        },
      ),
    );
  }
}
