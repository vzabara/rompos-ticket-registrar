import 'package:rompos_ticket_registrar/screens/auth_screen.dart';
import 'package:rompos_ticket_registrar/screens/verify_tickets_screen.dart';
import 'package:rompos_ticket_registrar/services/login_auth_service.dart';
import 'package:rompos_ticket_registrar/services/services_locator.dart';
import 'package:flutter/material.dart';

class AuthWidgetChecker extends StatelessWidget {
  const AuthWidgetChecker({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return StreamBuilder<String>(
      stream: getIt<LoginAuthService>().authState,
      builder: (BuildContext context, snapshot) {
        final String? token = snapshot.data;
        if (token!.isNotEmpty) {
          return VerifyTicketsScreen();
        }
        return AuthScreen();
      },
    );
  }
}
