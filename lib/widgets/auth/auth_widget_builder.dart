import 'package:rompos_ticket_registrar/services/login_auth_service.dart';
import 'package:rompos_ticket_registrar/services/services_locator.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class AuthWidgetBuilder extends StatelessWidget {
  const AuthWidgetBuilder({Key? key, required this.builder}) : super(key: key);
  final Widget Function(BuildContext, AsyncSnapshot) builder;

  @override
  Widget build(BuildContext context) {
    return StreamBuilder<String>(
      stream: getIt<LoginAuthService>().authState,
      builder: (BuildContext context, snapshot) {
        final String? token = snapshot.data;
        if (token!.isNotEmpty) {
          return MultiProvider(
            providers: [
              Provider<String>.value(value: token),
            ],
            child: builder(context, snapshot),
          );
        }
        return builder(context, snapshot);
      },
    );
  }
}
