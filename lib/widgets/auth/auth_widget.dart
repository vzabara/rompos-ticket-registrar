import 'package:rompos_ticket_registrar/screens/auth_screen.dart';
import 'package:flutter/material.dart';
import 'package:rompos_ticket_registrar/screens/verify_tickets_screen.dart';

// import '../../main.dart';

class AuthWidget extends StatelessWidget {
  const AuthWidget({Key? key, required this.token}) : super(key: key);
  final String token;

  @override
  Widget build(BuildContext context) {
    return token.isEmpty ? AuthScreen() : VerifyTicketsScreen();
    // if (apiToken == '') {
    //   return AuthScreen();
    // }
    // return Scaffold(
    //   body: Center(
    //     child: CircularProgressIndicator(),
    //   ),
    // );
  }
}
