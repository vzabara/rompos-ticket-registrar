import 'package:rompos_ticket_registrar/services/package_info_service.dart';
import 'package:rompos_ticket_registrar/services/services_locator.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class AuthForm extends StatefulWidget {
  final bool isLoading;
  final void Function(
    BuildContext ctx,
    String email,
    String password,
  ) submitFn;

  AuthForm(
    this.submitFn,
    this.isLoading,
  );

  @override
  _AuthFormState createState() => _AuthFormState();
}

class _AuthFormState extends State<AuthForm> {
  final _formKey = GlobalKey<FormState>();
  var _isPasswordVisible = false;
  var _userEmail = '';
  var _userPassword = '';

  void _trySubmit() {
    final isValid = _formKey.currentState!.validate();
    FocusScope.of(context).unfocus();

    if (isValid) {
      _formKey.currentState!.save();
      widget.submitFn(
        context,
        _userEmail.trim(),
        _userPassword.trim(),
      );
    }
  }

  final _usernameController = TextEditingController();
  final _passwordController = TextEditingController();

  @override
  void initState() {
    super.initState();
    _usernameController.addListener(() {
      setState(() {});
    });
    _passwordController.addListener(() {
      setState(() {});
    });
  }

  @override
  void dispose() {
    _usernameController.dispose();
    _passwordController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Consumer<PackageInfoService>(
      builder: (ctx, service, child) {
        return Center(
          child: SingleChildScrollView(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Container(
                  padding: EdgeInsets.symmetric(
                    horizontal: 30,
                    vertical: 20,
                  ),
                  width: 380,
                  child: Image.asset(
                    'assets/images/rompos_verifier_logo.png',
                    fit: BoxFit.fill,
                  ),
                ),
                Container(
                  width: 400,
                  child: Card(
                    margin: EdgeInsets.symmetric(horizontal: 20),
                    elevation: 10,
                    color: Colors.grey[50],
                    child: Padding(
                      padding: EdgeInsets.all(10),
                      child: FocusTraversalGroup(
                        child: Form(
                          key: _formKey,
                          child: Column(
                            mainAxisSize: MainAxisSize.min,
                            children: <Widget>[
                              TextFormField(
                                key: const ValueKey('username'),
                                autocorrect: false,
                                textCapitalization: TextCapitalization.none,
                                textInputAction: TextInputAction.next,
                                enableSuggestions: false,
                                validator: (value) {
                                  if (value!.isEmpty) {
                                    return 'Please enter username.';
                                  }
                                  return null;
                                },
                                keyboardType: TextInputType.emailAddress,
                                controller: _usernameController,
                                decoration: InputDecoration(
                                  labelText: 'Username',
                                  suffixIcon:
                                      _usernameController.text.length > 0
                                          ? IconButton(
                                              onPressed: () =>
                                                  _usernameController.clear(),
                                              icon: Icon(Icons.clear,
                                                  color: Colors.grey),
                                            )
                                          : null,
                                ),
                                onSaved: (value) {
                                  _userEmail = value!;
                                },
                              ),
                              TextFormField(
                                key: const ValueKey('password'),
                                validator: (value) {
                                  if (value!.isEmpty || value.length < 6) {
                                    return 'Password must be at least 6 characters long.';
                                  }
                                  return null;
                                },
                                controller: _passwordController,
                                decoration: InputDecoration(
                                  labelText: 'Password',
                                  suffixIcon:
                                      _passwordController.text.length > 0
                                          ? IconButton(
                                              onPressed: () {
                                                setState(() {
                                                  _isPasswordVisible =
                                                      !_isPasswordVisible;
                                                });
                                              },
                                              icon: Icon(Icons.remove_red_eye,
                                                  color: Colors.grey),
                                            )
                                          : null,
                                ),
                                obscureText: !_isPasswordVisible,
                                onSaved: (value) {
                                  _userPassword = value!;
                                },
                              ),
                              SizedBox(height: 12),
                              if (widget.isLoading) CircularProgressIndicator(),
                              if (!widget.isLoading)
                                TextButton(
                                  child: Text(
                                    'Login',
                                  ),
                                  onPressed: _trySubmit,
                                ),
                            ],
                          ),
                        ),
                      ),
                    ),
                  ),
                ),
                SizedBox(
                  height: 40,
                ),
                getIt<PackageInfoService>().version(),
              ],
            ),
          ),
        );
      },
    );
  }
}
