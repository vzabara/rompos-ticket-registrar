import 'package:rompos_ticket_registrar/models/schedule.dart';
import 'package:rompos_ticket_registrar/models/ticket.dart';
import 'package:rompos_ticket_registrar/services/http_service.dart';
import 'package:rompos_ticket_registrar/services/services_locator.dart';
import 'package:flutter/material.dart';
import 'package:rompos_ticket_registrar/widgets/tickets/ticket_item.dart';

class TicketsList extends StatelessWidget {
  final Schedule schedule;
  TicketsList(this.schedule);

  @override
  Widget build(BuildContext context) {
    return FutureBuilder<List<Ticket>?>(
      future: getIt<HttpService>().latestTickets(),
      builder: (ctx, snapshot) {
        if (snapshot.connectionState == ConnectionState.waiting) {
          return Center(
            child: CircularProgressIndicator(),
          );
        }
        if (!snapshot.hasData) {
          return Center(
            child: Text('No items'),
          );
        }
        return ListView.builder(
          itemCount: snapshot.data!.length,
          itemBuilder: (ctx, index) {
            return TicketItem(snapshot.data![index], schedule);
          },
        );
      },
    );
  }
}
