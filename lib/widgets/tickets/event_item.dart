import 'package:rompos_ticket_registrar/models/event.dart';
import 'package:flutter/material.dart';
import 'package:rompos_ticket_registrar/screens/event_arguments.dart';
import 'package:rompos_ticket_registrar/screens/schedules_screen.dart';

class EventItem extends StatelessWidget {
  final Event event;
  final String selectedDate;

  EventItem(this.event, this.selectedDate);

  @override
  Widget build(BuildContext context) {
    return Card(
      child: InkResponse(
        highlightShape: BoxShape.rectangle,
        highlightColor: Colors.blue.withOpacity(0.5),
        child: ListTile(
          title: Text(
            event.title,
            style: TextStyle(
              fontSize: 18,
              fontWeight: FontWeight.bold,
              color: Colors.black54,
            ),
          ),
          trailing: Icon(Icons.schedule),
        ),
        onTap: () {
          Navigator.pushNamed(
            context,
            SchedulesScreen.routeName,
            arguments: EventArguments(event, selectedDate),
          );
        },
      ),
    );
  }
}
