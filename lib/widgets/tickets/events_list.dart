import 'package:rompos_ticket_registrar/models/event.dart';
import 'package:rompos_ticket_registrar/services/http_service.dart';
import 'package:rompos_ticket_registrar/services/services_locator.dart';
import 'package:flutter/material.dart';
import 'package:rompos_ticket_registrar/widgets/tickets/event_item.dart';

class EventsList extends StatefulWidget {
  final String selectedDate;

  EventsList(this.selectedDate);

  @override
  State<EventsList> createState() => _EventsListState();
}

class _EventsListState extends State<EventsList> {
  bool _isLoadMoreRunning = false;

  var data;
  var events;
  int page = 1;
  ScrollController scrollController = ScrollController();
  List<Event> eventsList = [];

  getEvents() async {
    setState(() {
      _isLoadMoreRunning = true;
    });

    events = getIt<HttpService>().getActiveEvents(widget.selectedDate, page);

    page++;
    var newEvents = await events;
    eventsList.addAll(newEvents);

    setState(() {
      _isLoadMoreRunning = false;
    });
  }

  @override
  void initState() {
    super.initState();
    data = getEvents();
    scrollController.addListener(() {
      if (scrollController.position.pixels ==
          scrollController.position.maxScrollExtent) {
        setState(() {
          data = getEvents();
        });
      }
    });
  }

  @override
  void dispose() {
    super.dispose();
    eventsList = [];
  }

  @override
  Widget build(BuildContext context) {
    return eventsList.length == 0
        ? Center(child: Text('Empty list'))
        : Column(children: [
            Expanded(
              child: ListView.builder(
                controller: scrollController,
                itemCount: eventsList.length,
                itemBuilder: (ctx, index) {
                  return EventItem(eventsList[index], widget.selectedDate);
                },
              ),
            ),
            Visibility(
              visible: _isLoadMoreRunning,
              child: Padding(
                padding: const EdgeInsets.only(top: 10, bottom: 30),
                child: Center(
                  child: CircularProgressIndicator.adaptive(),
                ),
              ),
            ),
          ]);
  }
}
