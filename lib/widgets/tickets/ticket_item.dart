import 'package:rompos_ticket_registrar/models/schedule.dart';
import 'package:rompos_ticket_registrar/models/ticket.dart';
import 'package:flutter/material.dart';
import 'package:rompos_ticket_registrar/services/http_service.dart';
import 'package:rompos_ticket_registrar/services/services_locator.dart';

class TicketItem extends StatelessWidget {
  final Ticket ticket;
  final Schedule schedule;

  TicketItem(this.ticket, this.schedule);

  Color _getColor(status) {
    if (status == 'active') {
      return Colors.green;
    }
    return Colors.red;
  }

  @override
  Widget build(BuildContext context) {
    return Card(
      // elevation: 5,
      child: ListTile(
        title: Text(
          'Ticket ${ticket.productSaleId}-${ticket.id}',
          style: TextStyle(
            fontSize: 18,
            fontWeight: FontWeight.bold,
            color: Colors.black54,
          ),
        ),
        trailing: IconButton(
          icon: Icon(Icons.verified_outlined),
          onPressed: () {
            final dialog = AlertDialog(
              title: Text('Ticket ${ticket.productSaleId}-${ticket.id}'),
              content: Column(
                mainAxisSize: MainAxisSize.min,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  FutureBuilder<String?>(
                    future: getIt<HttpService>().verify(ticket, schedule),
                    builder: (ctx, snapshot) {
                      if (snapshot.connectionState == ConnectionState.waiting) {
                        return Center(
                          child: CircularProgressIndicator(),
                        );
                      }
                      if (!snapshot.hasData) {
                        return Center(
                          child: Text('Status is unknown'),
                        );
                      }
                      final status = snapshot.data;
                      return RichText(
                        text: TextSpan(
                          text: 'Status is ',
                          style: DefaultTextStyle.of(context).style,
                          children: <TextSpan>[
                            TextSpan(
                              text: status,
                              style: TextStyle(
                                fontWeight: FontWeight.bold,
                                color: _getColor(status),
                              ),
                            ),
                          ],
                        ),
                      );
                    },
                  ),
                ],
              ),
              actions: [
                TextButton(
                  child: Text('OK'),
                  onPressed: () =>
                      Navigator.of(context, rootNavigator: true).pop('dialog'),
                ),
              ],
            );
            showDialog(
              context: context,
              builder: (ctx) => dialog,
            );
          },
        ),
      ),
    );
  }
}
