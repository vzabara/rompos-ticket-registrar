import 'package:flutter/material.dart';
import 'package:rompos_ticket_registrar/models/schedule.dart';
import 'package:rompos_ticket_registrar/screens/ticket_scanner_screen.dart';

class ScheduleItem extends StatelessWidget {
  final Schedule schedule;

  ScheduleItem(this.schedule);

  @override
  Widget build(BuildContext context) {
    return Card(
      child: InkResponse(
        highlightShape: BoxShape.rectangle,
        highlightColor: Colors.blue.withOpacity(0.5),
        child: ListTile(
          title: Text(
            schedule.startTime,
            style: TextStyle(
              fontSize: 18,
              fontWeight: FontWeight.bold,
              color: Colors.black54,
            ),
          ),
          trailing: Icon(Icons.qr_code_2_outlined),
        ),
        onTap: () {
          Navigator.pushNamed(context, TicketScannerScreen.routeName,
              arguments: schedule);
        },
      ),
    );
  }
}
