import 'package:rompos_ticket_registrar/models/schedule.dart';
import 'package:rompos_ticket_registrar/screens/event_arguments.dart';
import 'package:rompos_ticket_registrar/services/http_service.dart';
import 'package:rompos_ticket_registrar/services/services_locator.dart';
import 'package:flutter/material.dart';
import 'package:rompos_ticket_registrar/widgets/tickets/schedule_item.dart';

class SchedulesList extends StatelessWidget {
  final EventArguments eventArgument;

  SchedulesList(this.eventArgument);
  @override
  Widget build(BuildContext context) {
    return FutureBuilder<List<Schedule>?>(
      future: getIt<HttpService>().getSchedules(eventArgument),
      builder: (ctx, snapshot) {
        if (snapshot.connectionState == ConnectionState.waiting) {
          return Center(
            child: CircularProgressIndicator(),
          );
        }
        if (!snapshot.hasData || snapshot.data!.length == 0) {
          return Center(
            child: Text('No items'),
          );
        }
        return ListView.builder(
          itemCount: snapshot.data!.length,
          itemBuilder: (ctx, index) {
            return ScheduleItem(snapshot.data![index]);
          },
        );
      },
    );
  }
}
