import 'package:flutter/material.dart';

import 'package:rompos_ticket_registrar/models/user_settings.dart';
import 'package:rompos_ticket_registrar/services/package_info_service.dart';
import 'package:rompos_ticket_registrar/services/services_locator.dart';

class SettingsForm extends StatefulWidget {
  final UserSettings _userSettings;
  final Function _saveSettingsHandler;

  SettingsForm(this._userSettings, this._saveSettingsHandler);

  @override
  _SettingsFormState createState() => _SettingsFormState();
}

class _SettingsFormState extends State<SettingsForm> {
  late UserSettings _settings;
  late TextEditingController _serverUrlController;

  @override
  void initState() {
    super.initState();
    _settings = widget._userSettings;
    _serverUrlController =
        TextEditingController(text: widget._userSettings.serverUrl);
    _serverUrlController.addListener(() {
      setState(() {});
    });
  }

  @override
  void dispose() {
    _serverUrlController.dispose();
    super.dispose();
  }

  void _setNewSettings(UserSettings settings) {
    setState(() {
      _settings = settings;
      widget._saveSettingsHandler(_settings);
    });
  }

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Padding(
        padding: EdgeInsets.symmetric(
          vertical: 10,
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Padding(
              padding: EdgeInsets.fromLTRB(15, 0, 15, 10),
              child: Text(
                'Server',
                style: TextStyle(
                  color: const Color(0xff446179),
                  fontSize: 22,
                  fontWeight: FontWeight.bold,
                ),
              ),
            ),
            Padding(
              padding: EdgeInsets.fromLTRB(15, 0, 15, 10),
              child: TextFormField(
                key: const ValueKey('serverUrl'),
                autocorrect: false,
                textCapitalization: TextCapitalization.none,
                textInputAction: TextInputAction.next,
                enableSuggestions: false,
                validator: (value) {
                  if (value!.isEmpty) {
                    return 'Please enter server URL';
                  }
                  return null;
                },
                controller: _serverUrlController,
                decoration: InputDecoration(
                  labelText: 'Server URL',
                  hintText: 'Enter your server URL',
                  suffixIcon: _serverUrlController.text.length > 0
                      ? IconButton(
                          onPressed: () => _serverUrlController.clear(),
                          icon: Icon(Icons.clear, color: Colors.grey),
                        )
                      : null,
                ),
                onChanged: (value) {
                  _settings = UserSettings(
                    serverUrl: value,
                    apiToken: _settings.apiToken,
                    automaticRegister: _settings.automaticRegister,
                    soundsEnabled: _settings.soundsEnabled,
                  );
                  _setNewSettings(_settings);
                },
              ),
            ),
            Divider(),
            Padding(
              padding: EdgeInsets.fromLTRB(15, 0, 15, 10),
              child: Text(
                'Register Tickets',
                style: TextStyle(
                  color: const Color(0xff446179),
                  fontSize: 22,
                  fontWeight: FontWeight.bold,
                ),
              ),
            ),
            ListTile(
              title: Text(
                'Automatically',
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                ),
              ),
              subtitle: Text(
                'Tickets are registered automatically after scanning',
              ),
              trailing: Switch.adaptive(
                value: _settings.automaticRegister,
                onChanged: (value) {
                  _settings = UserSettings(
                    serverUrl: _settings.serverUrl,
                    apiToken: _settings.apiToken,
                    automaticRegister: value,
                    soundsEnabled: _settings.soundsEnabled,
                  );
                  _setNewSettings(_settings);
                },
              ),
            ),
            Divider(),
            Padding(
              padding: EdgeInsets.fromLTRB(15, 0, 15, 10),
              child: Text(
                'Sound',
                style: TextStyle(
                  color: const Color(0xff446179),
                  fontSize: 22,
                  fontWeight: FontWeight.bold,
                ),
              ),
            ),
            ListTile(
              title: Text(
                'Enable',
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                ),
              ),
              subtitle: Text(
                'Enable/disable sound on ticket scanning',
              ),
              trailing: Switch.adaptive(
                value: _settings.soundsEnabled,
                onChanged: (value) {
                  _settings = UserSettings(
                    serverUrl: _settings.serverUrl,
                    apiToken: _settings.apiToken,
                    automaticRegister: _settings.automaticRegister,
                    soundsEnabled: value,
                  );
                  _setNewSettings(_settings);
                },
              ),
            ),
            Divider(height: 40),
            Padding(
              padding: EdgeInsets.fromLTRB(15, 0, 15, 10),
              child: getIt<PackageInfoService>().version(),
            ),
          ],
        ),
      ),
    );
  }
}
