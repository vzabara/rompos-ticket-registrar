import 'package:rompos_ticket_registrar/screens/auth_screen.dart';
import 'package:rompos_ticket_registrar/screens/events_screen.dart';
import 'package:flutter/material.dart';
import 'package:rompos_ticket_registrar/screens/settings_screen.dart';
import 'package:rompos_ticket_registrar/services/login_auth_service.dart';
import 'package:rompos_ticket_registrar/services/services_locator.dart';
import 'package:platform/platform.dart';

class NavTabIndexes {
  static const int registrar = 0;
  static const int settings = 1;
  static const int exit = 2;
}

class TabBottomPadding {
  static const double ios = 20.0;
  static const double android = 10.0;
}

class NavigationTabBar extends StatefulWidget {
  final int selectedIndex;

  NavigationTabBar(this.selectedIndex);

  static const double TabIconSize = 32.0;

  @override
  _NavigationTabBarState createState() => _NavigationTabBarState();
}

class _NavigationTabBarState extends State<NavigationTabBar>
    with SingleTickerProviderStateMixin {
  late TabController _controller;
  List<Widget> _tabsList = [];

  List<Widget> _buildTabs() {
    _tabsList = [
      Tab(
        icon: Icon(
          Icons.qr_code_2_rounded,
          size: NavigationTabBar.TabIconSize,
        ),
        text: 'Scanner',
      ),
      Tab(
        icon: Icon(
          Icons.settings,
          size: NavigationTabBar.TabIconSize,
        ),
        text: SettingsScreen.title,
      ),
      Tab(
        icon: Icon(
          Icons.exit_to_app,
          size: NavigationTabBar.TabIconSize,
        ),
        text: 'Exit',
      ),
    ];
    return _tabsList;
  }

  @override
  void initState() {
    super.initState();
    _tabsList = _buildTabs();
    _controller = TabController(
      initialIndex: widget.selectedIndex,
      length: _tabsList.length,
      vsync: this,
    );
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  void _doAction(int index) {
    switch (index) {
      case NavTabIndexes.registrar:
        Navigator.pushReplacementNamed(context, EventsScreen.routeName);
        break;
      case NavTabIndexes.settings:
        Navigator.pushReplacementNamed(context, SettingsScreen.routeName);
        break;
      case NavTabIndexes.exit:
        showDialog(
          context: context,
          builder: (BuildContext context) {
            return AlertDialog(
              title: Text('Exit'),
              content: Text('Are you sure?'),
              actions: [
                TextButton(
                  child: Text('No'),
                  onPressed: () {
                    // restore original index
                    _controller.index = widget.selectedIndex;
                    Navigator.pop(context);
                  },
                ),
                TextButton(
                  child: Text('Yes'),
                  onPressed: () async {
                    await getIt<LoginAuthService>().signOut();
                    Navigator.pushReplacementNamed(
                        context, AuthScreen.routeName);
                  },
                ),
              ],
            );
          },
        );
        break;
    }
  }

  @override
  Widget build(BuildContext context) {
    final bottomPadding =
        LocalPlatform().isIOS ? TabBottomPadding.ios : TabBottomPadding.android;
    return TabBar(
      controller: _controller,
      tabs: _buildTabs(),
      labelColor: Colors.blueGrey[800],
      labelStyle: TextStyle(
        fontSize: 12,
      ),
      unselectedLabelColor: Colors.blueGrey[300],
      indicatorSize: TabBarIndicatorSize.label,
      indicatorColor: Colors.red,
      indicatorPadding: EdgeInsets.only(
        bottom: bottomPadding,
      ),
      labelPadding: EdgeInsets.fromLTRB(
        2,
        2,
        2,
        bottomPadding,
      ),
      onTap: (index) => _doAction(index),
    );
  }
}
