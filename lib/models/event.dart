class Event {
  int id;
  String title;
  String? description;
  DateTime? startDate;
  DateTime? endDate;
  bool isActive;

  Event({
    this.id = 0,
    required this.title,
    required this.description,
    required this.startDate,
    required this.endDate,
    this.isActive = false,
  });

  Event.fromMap(Map<String, dynamic> map)
      : id = map['id'],
        title = map['title'],
        description = map['description'],
        startDate = DateTime.parse(map['start_date']),
        endDate = DateTime.parse(map['end_date']),
        isActive = map['is_active'];

  Map<String, dynamic> toMap() {
    var map = <String, dynamic>{
      'id': id,
      'title': title,
      'start_date': startDate,
      'end_date': endDate,
      'is_active': isActive,
    };
    return map;
  }
}
