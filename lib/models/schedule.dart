class Schedule {
  int id;
  int eventDateId;
  double price;
  String startTime;
  int? ticketNumber;
  bool isActive;
  String? title;

  Schedule({
    this.id = 0,
    required this.title,
    required this.eventDateId,
    required this.price,
    required this.startTime,
    required this.ticketNumber,
    this.isActive = false,
  });

  Schedule.fromMap(Map<String, dynamic> map)
      : id = map['id'],
        title = map['title'],
        eventDateId = map['event_date_id'],
        startTime = map['start_time'],
        price = double.parse(map['price']),
        ticketNumber = map['ticket_number'],
        isActive = map['is_active'];

  Map<String, dynamic> toMap() {
    var map = <String, dynamic>{
      'id': id,
      'title': title,
      'event_date_id': eventDateId,
      'start_time': startTime,
      'price': price,
      'ticket_number': ticketNumber,
      'is_active': isActive,
    };
    return map;
  }
}
