///
/// Use this constants to define Hive type ID to generate custom data adapters.
/// Example:
///
/// part 'current_user.g.dart';
/// @HiveType(typeId: HiveTypes.user)
/// class CurrentUser {
///   @HiveField(0)
///   final String userId;
///   @HiveField(1)
///   final String name;
///   @HiveField(2)
///   final String email;
///   @HiveField(3)
///   final String avatar;
///   @HiveField(4)
///   final String method;
///
///
///  CurrentUser({
///    this.userId = '',
///    this.name = 'Anonymous',
///    this.email = '',
///    this.avatar = '',
///    this.method = SigninMethod.Email,
///  });
///}
/// Then generate adapter with command:
///
/// flutter packages pub run build_runner build --delete-conflicting-outputs
///
/// For more details see https://docs.hivedb.dev/
///

class HiveTypes {
  static const user = 0;
  static const settings = 1;
  static const intro = 2;
}
