class Ticket {
  int id;
  int eventScheduleId;
  int? productSaleId;
  int customerId;
  bool isUsed;

  Ticket({
    this.id = 0,
    this.eventScheduleId = 1,
    this.productSaleId = 1,
    this.customerId = 1,
    this.isUsed = false,
  });

  Ticket.fromMap(Map<String, dynamic> map)
      : id = map['id'],
        eventScheduleId = map['event_schedule_id'],
        productSaleId = map['product_sale_id'],
        customerId = map['customer_id'],
        isUsed = map['is_used'];

  Map<String, dynamic> toMap() {
    var map = <String, dynamic>{
      'id': id,
      'event_schedule_id': eventScheduleId,
      'product_sale_id': productSaleId,
      'customer_id': customerId,
      'is_used': isUsed,
    };
    return map;
  }
}
