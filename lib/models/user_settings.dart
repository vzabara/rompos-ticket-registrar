import 'package:hive/hive.dart';

import 'package:rompos_ticket_registrar/models/hive_types.dart';

part 'user_settings.g.dart';

@HiveType(typeId: HiveTypes.settings)
class UserSettings {
  @HiveField(0)
  String? serverUrl;
  @HiveField(1)
  String? apiToken;
  @HiveField(2)
  bool automaticRegister;
  @HiveField(3)
  bool soundsEnabled;

  UserSettings({
    this.serverUrl = 'https://posv2.rompos.com',
    this.apiToken = '',
    this.automaticRegister = true,
    this.soundsEnabled = true,
  });

  UserSettings.fromMap(Map<String, dynamic> json)
      : serverUrl = json['serverUrl'],
        apiToken = json['apiToken'],
        automaticRegister = json['automaticRegister'] ?? 1,
        soundsEnabled = json['soundsEnabled'] ?? 1;

  Map<String, dynamic> toMap() {
    return {
      'serverUrl': serverUrl,
      'apiToken': apiToken,
      'automaticRegister': automaticRegister,
      'soundsEnabled': soundsEnabled,
    };
  }
}
