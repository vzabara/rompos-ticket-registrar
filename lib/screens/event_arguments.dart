import 'package:rompos_ticket_registrar/models/event.dart';

class EventArguments {
  final Event event;
  final String date;

  EventArguments(this.event, this.date);
}
