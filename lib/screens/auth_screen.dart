import 'package:rompos_ticket_registrar/services/login_auth_service.dart';
import 'package:rompos_ticket_registrar/services/services_locator.dart';
import 'package:rompos_ticket_registrar/widgets/auth/auth_form.dart';
import 'package:flutter/material.dart';

class AuthScreen extends StatefulWidget {
  static const routeName = '/';

  @override
  _AuthScreenState createState() => _AuthScreenState();
}

class _AuthScreenState extends State<AuthScreen> {
  var _isLoading = false;

  void _submitAuthForm(
    BuildContext ctx,
    String username,
    String password,
  ) async {
    String message = '';
    final authService = getIt<LoginAuthService>();
    try {
      setState(() {
        _isLoading = true;
      });
      await authService.signIn(username, password);
    } catch (error) {
      message = error.toString();
    } finally {
      if (mounted) {
        if (message.isNotEmpty) {
          ScaffoldMessenger.of(context).hideCurrentSnackBar();
          ScaffoldMessenger.of(context).showSnackBar(
            SnackBar(
              backgroundColor: const Color(0x66446179),
              content: Text(message),
            ),
          );
        }
        setState(() {
          _isLoading = false;
        });
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.grey[50],
      body: AuthForm(
        _submitAuthForm,
        _isLoading,
      ),
    );
  }
}
