import 'dart:io';

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:qr_code_scanner/qr_code_scanner.dart';
import 'package:rompos_ticket_registrar/main.dart';
import 'package:rompos_ticket_registrar/models/schedule.dart';
import 'package:rompos_ticket_registrar/services/http_service.dart';
import 'package:rompos_ticket_registrar/services/profile_service.dart';
import 'package:rompos_ticket_registrar/services/services_locator.dart';
import 'package:flutter_beep/flutter_beep.dart';

import 'events_screen.dart';

class TicketScannerScreen extends StatefulWidget {
  static const routeName = '/scanner';
  static String title = 'Ticket Scanner';

  @override
  _TicketScannerScreenState createState() => _TicketScannerScreenState();
}

class _TicketScannerScreenState extends State<TicketScannerScreen> {
  Barcode? result;
  QRViewController? controller;
  final GlobalKey qrKey = GlobalKey(debugLabel: 'QR');
  late Schedule schedule;
  late String modeLabel;

  void _getModeLabel() {
    modeLabel = settings!.automaticRegister ? 'Automatic' : 'Manual';
  }

  @override
  void initState() {
    super.initState();
    if (Platform.isIOS) {}
    _getModeLabel();
  }

  @override
  void dispose() {
    controller?.dispose();
    super.dispose();
  }

  // In order to get hot reload to work we need to pause the camera if the platform
  // is android, or resume the camera if the platform is iOS.
  @override
  void reassemble() {
    super.reassemble();
    if (Platform.isAndroid) {
      controller!.pauseCamera();
    }
    controller!.resumeCamera();
  }

  Future<void> _checkTicket(Barcode result) async {
    await controller?.pauseCamera();
    var pos = result.code.indexOf('tickets/verify/');
    if (pos != -1) {
      final String hash = result.code.substring(pos + 15);
      final List<String> parts = hash.split('-').toList();
      final dialog = AlertDialog(
        title: Text('Checking ticket'),
        content: Column(
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            FutureBuilder<String?>(
              future: getIt<HttpService>()
                  .verify2('${parts[0]}-${parts[1]}', schedule),
              builder: (ctx, snapshot) {
                if (snapshot.connectionState == ConnectionState.waiting) {
                  return Center(
                    child: CircularProgressIndicator(),
                  );
                }
                if (!snapshot.hasData) {
                  if (settings!.soundsEnabled) {
                    FlutterBeep.beep(false);
                  }
                  return Center(
                    child: Text('Ticket status is unknown'),
                  );
                }
                if (snapshot.data == 'active') {
                  if (settings!.soundsEnabled) {
                    FlutterBeep.beep();
                  }
                  if (settings!.automaticRegister) {
                    // send register ticket request
                    getIt<HttpService>().register('${parts[0]}-${parts[1]}');
                    return Text('Ticket is Registered');
                  } else {
                    // ask user what to do
                    return Center(
                      child: ElevatedButton(
                        style: ElevatedButton.styleFrom(
                          // primary: Colors.orange.shade800, // background
                          onPrimary: Colors.white, // foreground
                          padding: EdgeInsets.symmetric(
                            vertical: 20,
                            horizontal: 40,
                          ),
                          textStyle: TextStyle(
                            fontSize: 20,
                          ),
                        ),
                        onPressed: () async {
                          getIt<HttpService>()
                              .register('${parts[0]}-${parts[1]}');
                          await controller?.resumeCamera();
                          Navigator.of(context, rootNavigator: true)
                              .pop('dialog');
                        },
                        child: Text('Register'),
                      ),
                    );
                  }
                } else {
                  if (settings!.soundsEnabled) {
                    FlutterBeep.beep(false);
                  }
                  late String message;
                  switch (snapshot.data) {
                    case 'inactive':
                      message = 'Ticket is already registered';
                      break;
                    case 'not_found':
                      message = 'Ticket is not found';
                      break;
                    case 'wrong_schedule':
                    default:
                      message = 'Ticket status is unknown';
                      break;
                  }
                  return Text(message);
                }
              },
            ),
          ],
        ),
        // actions: [
        //   TextButton(
        //     child: Text('OK'),
        //     onPressed: () async {
        //       await controller?.resumeCamera();
        //       Navigator.of(context, rootNavigator: true).pop('dialog');
        //     },
        //   ),
        // ],
      );
      showDialog(
        context: context,
        builder: (ctx) => dialog,
      ).then((exit) async {
        await controller?.resumeCamera();
      });
    } else {
      await controller?.resumeCamera();
    }
  }

  @override
  Widget build(BuildContext context) {
    schedule = ModalRoute.of(context)!.settings.arguments as Schedule;
    if (result != null) {
      _checkTicket(result!);
    }
    return Scaffold(
      body: Column(
        children: <Widget>[
          Expanded(
            flex: 4,
            child: _buildQrView(context),
          ),
          Expanded(
            flex: 2,
            child: FittedBox(
              fit: BoxFit.fitHeight,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: <Widget>[
                  Padding(
                    padding: EdgeInsets.only(top: 10),
                    child: Text(
                      'Scan a ticket..',
                      style: TextStyle(
                        fontSize: 20,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      Container(
                        margin: EdgeInsets.all(8),
                        child: ElevatedButton(
                          style: ElevatedButton.styleFrom(
                            primary: Colors.blue.shade400, // background
                            onPrimary: Colors.white, // foreground
                            padding: EdgeInsets.symmetric(
                              vertical: 10,
                              horizontal: 10,
                            ),
                          ),
                          onPressed: () async {
                            settings!.soundsEnabled = !settings!.soundsEnabled;
                            setState(() {});
                          },
                          child: Builder(
                            builder: (context) {
                              var label = settings!.soundsEnabled == true
                                  ? 'On'
                                  : 'Off';
                              return Text('Sound: $label');
                            },
                          ),
                        ),
                      ),
                      Container(
                        margin: EdgeInsets.all(8),
                        child: ElevatedButton(
                          style: ElevatedButton.styleFrom(
                            primary: Colors.orange.shade800, // background
                            onPrimary: Colors.white, // foreground
                            padding: EdgeInsets.symmetric(
                              vertical: 10,
                              horizontal: 10,
                            ),
                          ),
                          onPressed: () {
                            setState(() {
                              settings!.automaticRegister =
                                  !settings!.automaticRegister;
                              getIt<ProfileService>().saveSettings(settings!);
                              _getModeLabel();
                            });
                          },
                          child: Text('Register Mode: $modeLabel'),
                        ),
                      ),
                    ],
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      Container(
                        margin: EdgeInsets.all(8),
                        child: ElevatedButton(
                          onPressed: () async {
                            await controller?.toggleFlash();
                            setState(() {});
                          },
                          child: FutureBuilder(
                            future: controller?.getFlashStatus(),
                            builder: (context, snapshot) {
                              var label = snapshot.data == true ? 'On' : 'Off';
                              return Text('Flash: $label');
                            },
                          ),
                        ),
                      ),
                      Container(
                        margin: EdgeInsets.all(8),
                        child: ElevatedButton(
                          onPressed: () async {
                            await controller?.flipCamera();
                            setState(() {});
                          },
                          child: FutureBuilder(
                            future: controller?.getCameraInfo(),
                            builder: (context, snapshot) {
                              if (snapshot.data != null) {
                                return Text(
                                    'Camera: ${describeEnum(snapshot.data!)}');
                              } else {
                                return Text('loading');
                              }
                            },
                          ),
                        ),
                      )
                    ],
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      Container(
                        margin: EdgeInsets.all(8),
                        child: ElevatedButton(
                          style: ElevatedButton.styleFrom(
                            primary: Colors.red.shade900, // background
                            onPrimary: Colors.white, // foreground
                            padding: EdgeInsets.symmetric(
                              vertical: 10,
                              horizontal: 20,
                            ),
                            textStyle: TextStyle(
                              fontSize: 16,
                            ),
                          ),
                          onPressed: () async {
                            await controller?.pauseCamera();
                            Navigator.pop(context, EventsScreen.routeName);
                          },
                          child: Text('Close'),
                        ),
                      ),
                      Container(
                        margin: EdgeInsets.all(8),
                        child: ElevatedButton(
                          style: ElevatedButton.styleFrom(
                            primary: Colors.yellow.shade900, // background
                            onPrimary: Colors.white, // foreground
                            padding: EdgeInsets.symmetric(
                              vertical: 10,
                              horizontal: 20,
                            ),
                            textStyle: TextStyle(
                              fontSize: 16,
                            ),
                          ),
                          onPressed: () async {
                            await controller?.pauseCamera();
                          },
                          child: Text('Pause'),
                        ),
                      ),
                      Container(
                        margin: EdgeInsets.all(8),
                        child: ElevatedButton(
                          style: ElevatedButton.styleFrom(
                            primary: Colors.green.shade900, // background
                            onPrimary: Colors.white,
                            padding: EdgeInsets.symmetric(
                              vertical: 10,
                              horizontal: 20,
                            ),
                            textStyle: TextStyle(
                              fontSize: 16,
                            ),
                          ),
                          onPressed: () async {
                            await controller?.resumeCamera();
                          },
                          child: Text('Resume'),
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            ),
          )
        ],
      ),
    );
  }

  Widget _buildQrView(BuildContext context) {
    // For this example we check how width or tall the device is and change the scanArea and overlay accordingly.
    var scanArea = MediaQuery.of(context).size.width * 0.8;
    return QRView(
      key: qrKey,
      onQRViewCreated: _onQRViewCreated,
      overlay: QrScannerOverlayShape(
          borderColor: Colors.red,
          borderRadius: 10,
          borderLength: 30,
          borderWidth: 10,
          cutOutSize: scanArea),
      onPermissionSet: (ctrl, p) => _onPermissionSet(context, ctrl, p),
    );
  }

  void _onQRViewCreated(QRViewController controller) {
    setState(() {
      this.controller = controller;
    });
    controller.scannedDataStream.listen((scanData) {
      setState(() {
        result = scanData;
      });
    });
  }

  void _onPermissionSet(BuildContext context, QRViewController ctrl, bool p) {
    if (!p) {
      ScaffoldMessenger.of(context).showSnackBar(
        SnackBar(content: Text('No Permission')),
      );
    }
  }
}
