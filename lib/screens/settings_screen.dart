import 'package:flutter/material.dart';
import 'package:rompos_ticket_registrar/models/user_settings.dart';

import 'package:rompos_ticket_registrar/services/profile_service.dart';
import 'package:rompos_ticket_registrar/services/services_locator.dart';
import 'package:rompos_ticket_registrar/widgets/navigation_tab_bar.dart';
import 'package:rompos_ticket_registrar/widgets/settings/settings_form.dart';

import '../main.dart';

class SettingsScreen extends StatefulWidget {
  static const routeName = '/settings';

  static String title = 'Settings';

  @override
  _SettingsScreenState createState() => _SettingsScreenState();
}

class _SettingsScreenState extends State<SettingsScreen> {
  final _scaffoldKey = GlobalKey<ScaffoldState>();

  late UserSettings userSettings;

  @override
  void initState() {
    super.initState();
    userSettings = settings!;
  }

  void _saveSettingsModel(UserSettings settings) {
    userSettings = settings;
  }

  Future<void> _saveSettings() async {
    final profileService = getIt<ProfileService>();
    profileService.saveSettings(userSettings);
    ScaffoldMessenger.of(context).hideCurrentSnackBar();
    ScaffoldMessenger.of(context).showSnackBar(
      SnackBar(
        backgroundColor: const Color(0x66446179),
        content: Text('Settings have been saved'),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      appBar: AppBar(
        title: Text(SettingsScreen.title),
        iconTheme: IconThemeData(color: Colors.white),
        backgroundColor: const Color(0xff446179),
        brightness: Brightness.dark,
        actions: [
          IconButton(
            icon: Icon(Icons.save),
            onPressed: _saveSettings,
          )
        ],
      ),
      bottomNavigationBar: NavigationTabBar(NavTabIndexes.settings),
      body: Builder(
        builder: (ctx) {
          userSettings = getIt<ProfileService>().getSettings();
          return SafeArea(
            child: SettingsForm(
              userSettings,
              _saveSettingsModel,
            ),
          );
        },
      ),
    );
  }
}
