import 'package:flutter/material.dart';
import 'package:rompos_ticket_registrar/models/schedule.dart';
import 'package:rompos_ticket_registrar/widgets/tickets/tickets_list.dart';

class VerifyTicketsScreen extends StatelessWidget {
  static const routeName = '/verify';
  static String title = 'Verify Tickets';

  @override
  Widget build(BuildContext context) {
    Schedule schedule = ModalRoute.of(context)!.settings.arguments as Schedule;
    return Scaffold(
      appBar: AppBar(
        title: Text(title),
        iconTheme: IconThemeData(color: Colors.white),
        backgroundColor: const Color(0xff446179),
        brightness: Brightness.dark,
      ),
      body: TicketsList(schedule),
    );
  }
}
