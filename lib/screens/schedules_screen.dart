import 'package:flutter/material.dart';
import 'package:rompos_ticket_registrar/screens/event_arguments.dart';
import 'package:rompos_ticket_registrar/widgets/tickets/schedules_list.dart';

class SchedulesScreen extends StatelessWidget {
  static const routeName = '/schedules';
  static String title = 'Schedules';

  @override
  Widget build(BuildContext context) {
    final args = ModalRoute.of(context)!.settings.arguments as EventArguments;
    return Scaffold(
      appBar: AppBar(
        title: Text(title),
        iconTheme: IconThemeData(color: Colors.white),
        backgroundColor: const Color(0xff446179),
        brightness: Brightness.dark,
      ),
      body: SchedulesList(args),
    );
  }
}
