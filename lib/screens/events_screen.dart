import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:rompos_ticket_registrar/widgets/navigation_tab_bar.dart';
import 'package:rompos_ticket_registrar/widgets/tickets/events_list.dart';

class EventsScreen extends StatefulWidget {
  static const routeName = '/events';
  static String title = 'Events';

  @override
  _EventsScreenState createState() => _EventsScreenState();
}

class _EventsScreenState extends State<EventsScreen> {
  DateTime selectedDate = DateTime.now();
  final dateRoyalFormatter = DateFormat('yMMMMd');
  final dateMilitaryFormatter = DateFormat('yyyy-MM-dd');

  Future<void> _selectDate(BuildContext context) async {
    final DateTime? picked = await showDatePicker(
      context: context,
      initialDate: selectedDate,
      firstDate: DateTime(DateTime.now().year),
      lastDate: DateTime(DateTime.now().year + 1),
    );
    if (picked != null && picked != selectedDate)
      setState(() {
        selectedDate = picked;
      });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Events on ${dateRoyalFormatter.format(selectedDate)}'),
        iconTheme: IconThemeData(color: Colors.white),
        backgroundColor: const Color(0xff446179),
        brightness: Brightness.dark,
        actions: [
          IconButton(
            icon: Icon(Icons.calendar_today_outlined),
            onPressed: () => _selectDate(context),
          ),
        ],
      ),
      body: EventsList(dateMilitaryFormatter.format(selectedDate)),
      bottomNavigationBar: NavigationTabBar(NavTabIndexes.registrar),
    );
  }
}
