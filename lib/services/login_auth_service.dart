import 'dart:async';

import 'package:flutter/material.dart';
import 'package:rompos_ticket_registrar/models/user_settings.dart';
import 'package:rompos_ticket_registrar/services/profile_service.dart';
import 'package:rompos_ticket_registrar/services/services_locator.dart';

import 'http_service.dart';

class LoginAuthService with ChangeNotifier {
  Stream<String> get authState => _authStatus();
  String token = '';

  Future<void> signIn(String username, String password) async {
    token = (await getIt<HttpService>().login(username, password))!;
    saveUser();
  }

  Future<void> signOut() async {
    token = '';
    saveUser();
  }

  void saveUser() {
    final profileService = getIt<ProfileService>();
    UserSettings settings = profileService.getSettings();
    settings.apiToken = token;
    profileService.saveSettings(settings);
    notifyListeners();
  }

  Stream<String> _authStatus() async* {
    yield token;
  }
}
