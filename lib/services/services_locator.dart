import 'package:rompos_ticket_registrar/services/login_auth_service.dart';
import 'package:rompos_ticket_registrar/services/package_info_service.dart';
import 'package:get_it/get_it.dart';
import 'package:rompos_ticket_registrar/services/profile_service.dart';

import 'http_service.dart';

GetIt getIt = GetIt.instance;

void setupLocator() {
  getIt.registerSingleton<ProfileService>(ProfileService());
  getIt.registerSingleton<PackageInfoService>(PackageInfoService());
  getIt.registerSingleton<LoginAuthService>(LoginAuthService());
  getIt.registerSingleton<HttpService>(HttpService());
}
