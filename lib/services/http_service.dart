import 'dart:convert' as convert;

import 'package:http/http.dart' as http;
import 'package:rompos_ticket_registrar/models/event.dart';
import 'package:rompos_ticket_registrar/models/schedule.dart';
import 'package:rompos_ticket_registrar/models/ticket.dart';
import 'package:rompos_ticket_registrar/screens/event_arguments.dart';

import '../main.dart';

class HttpService {
  int apiVersion = 1;
  String get apiEndPoints => '${settings!.serverUrl}/api/v$apiVersion';

  Map<String, String> get headers => {
        'Accept': 'application/json',
        'Content-type': 'application/json',
      };

  Map<String, String> get authHeaders => {
        'Accept': 'application/json',
        'Content-type': 'application/json',
        'Authorization': 'Bearer ${settings!.apiToken}',
      };

  Future<String?> login(String username, String password) async {
    final response = await http.post(
      Uri.parse('$apiEndPoints/login'),
      headers: headers,
      body: convert.jsonEncode(
        {
          'name': username,
          'password': password,
        },
      ),
    );
    if (response.statusCode < 400) {
      var jsonResponse = convert.jsonDecode(response.body);
      if (jsonResponse['user'] != null) {
        return jsonResponse['user']['api_token'];
      }
    } else {
      throw new Exception('User was not found');
    }
  }

  Future<List<Event>?> getActiveEvents(String date, int page) async {
    try {
      // NOTE: pager is not yet implemented at server side
      final response = await http.post(
        Uri.parse('$apiEndPoints/events/get-active/$date?page=$page'),
        headers: authHeaders,
      );

      if (response.statusCode < 400) {
        var jsonResponse = convert.jsonDecode(response.body);
        Iterable list = jsonResponse['data']!;
        return list.map((model) => Event.fromMap(model)).toList();
      }
    } catch (error) {
      //return List.empty();
    }
    return List.empty();
  }

  Future<List<Schedule>?> getSchedules(EventArguments eventArgument) async {
    try {
      final response = await http.post(
        Uri.parse(
            '$apiEndPoints/event/${eventArgument.event.id}/${eventArgument.date}/get-active'),
        headers: authHeaders,
      );
      if (response.statusCode < 400) {
        var jsonResponse = convert.jsonDecode(response.body);
        Iterable list = jsonResponse['data']!;
        return list.map((model) => Schedule.fromMap(model)).toList();
      }
    } catch (error) {
      //return List.empty();
    }
    return List.empty();
  }

  Future<List<Ticket>?> latestTickets() async {
    try {
      final response = await http.get(
        Uri.parse('$apiEndPoints/tickets-latest'),
        headers: authHeaders,
      );
      if (response.statusCode < 400) {
        var jsonResponse = convert.jsonDecode(response.body);
        Iterable list = jsonResponse['data']!;
        return list.map((model) => Ticket.fromMap(model)).toList();
      }
    } catch (error) {
      //return List.empty();
    }
    return List.empty();
  }

  Future<String?> verify(Ticket ticket, Schedule schedule) async {
    try {
      final response = await http.get(
        Uri.parse(
            '$apiEndPoints/tickets/${ticket.productSaleId}-${ticket.id}/schedule/${schedule.id}'),
        headers: authHeaders,
      );
      if (response.statusCode < 400) {
        var jsonResponse = convert.jsonDecode(response.body);
        return jsonResponse['status']!;
      }
    } catch (error) {
      //return 'unknown';
    }
    return 'unknown';
  }

  Future<String?> verify2(String hash, Schedule schedule) async {
    try {
      final response = await http.get(
        Uri.parse('$apiEndPoints/tickets/$hash/schedule/${schedule.id}'),
        headers: authHeaders,
      );
      if (response.statusCode < 400) {
        var jsonResponse = convert.jsonDecode(response.body);
        return jsonResponse['status']!;
      }
    } catch (error) {
      //return 'unknown';
    }
    return 'unknown';
  }

  Future<bool> register(String hash) async {
    try {
      final response = await http.post(
        Uri.parse('$apiEndPoints/tickets/$hash'),
        headers: authHeaders,
      );
      if (response.statusCode < 400) {
        var jsonResponse = convert.jsonDecode(response.body);
        return jsonResponse['success']!;
      }
    } catch (error) {
      //return false;
    }
    return false;
  }
}
