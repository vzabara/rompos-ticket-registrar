import 'package:flutter/material.dart';
import 'package:package_info/package_info.dart';

class PackageInfoService with ChangeNotifier {
  PackageInfo _packageInfo = PackageInfo(
    appName: '',
    packageName: '',
    version: '',
    buildNumber: '',
  );

  Stream<PackageInfo> get packageInfo async* {
    yield _packageInfo;
    notifyListeners();
  }

  PackageInfoService() {
    _initPackageInfo();
  }

  Future<void> _initPackageInfo() async {
    final PackageInfo info = await PackageInfo.fromPlatform();
    _packageInfo = info;
  }

  version() => StreamBuilder(
        stream: packageInfo,
        builder: (ctx, AsyncSnapshot<PackageInfo> snapshot) {
          if (snapshot.data == null) {
            return Container();
          }
          return Text(
            'Version ${snapshot.data!.version}',
            style: TextStyle(
              color: Colors.blueGrey,
              fontSize: 12,
            ),
          );
        },
      );
}
