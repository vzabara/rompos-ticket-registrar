import 'package:hive/hive.dart';

import 'package:rompos_ticket_registrar/models/user_settings.dart';
import 'package:rompos_ticket_registrar/services/hive_boxes.dart';

class ProfileService {
  late Box<UserSettings> _userSettings;
  late UserSettings _settings;

  void saveSettings(UserSettings settings) {
    _settings = settings;
    _userSettings = Hive.box<UserSettings>(HiveBoxes.settings);
    _userSettings.put('settings', _settings);
  }

  UserSettings getSettings() {
    _userSettings = Hive.box<UserSettings>(HiveBoxes.settings);
    if (_userSettings.isNotEmpty) {
      _settings = _userSettings.get('settings')!;
    } else {
      _settings = UserSettings();
      saveSettings(_settings);
    }
    return _settings;
  }

  Future<void> exit() async {
    await Hive.box<UserSettings>(HiveBoxes.settings).clear();
  }
}
